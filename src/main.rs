use rand::Rng;
use itertools::Itertools;

fn main() {
    println!("{}", "Your lottery numbers are...");

    let mut number_array = [0u32; 5];

    for element in &mut number_array {
        *element = rand::thread_rng().gen_range(1, 70);
    }

    number_array.sort();

    println!("{}", number_array.iter().join(" "));

    let number_powerball = rand::thread_rng().gen_range(1, 27);
    println!("Powerball: {}", number_powerball);
}